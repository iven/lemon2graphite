Summary: export local LEMON data to Carbon / Graphite
Name: lemon2graphite
Version: 0.3
Release: 1%{?dist}
URL: http://cern.ch/lemon
License: GPL
Group: System/Utilities
BuildRoot: %{_tmppath}/%{name}-root
Requires: lemon-agent
Requires: python-daemon
Source0: lemon2graphite.py
Source1: carbon.csv
Source2: lemon2graphite.sysconfig
Source3: lemon2graphite.init
Source4: lemon2graphite.systemd
BuildArch: noarch

%if 0%{?rhel} >= 7
  %define _want_systemd 1
%{?systemd_requires}
BuildRequires: systemd
%else
  %define _want_systemd 0
%endif

%description
This package contains a utility to export local LEMON data to a remote
Carbon/Graphite server.

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/{sysconfig,lemon}
mkdir -p $RPM_BUILD_ROOT%{_sbindir}

install -m 755 %{SOURCE0} $RPM_BUILD_ROOT/%{_sbindir}/lemon2graphite
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/lemon/
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/lemon2graphite
%if %_want_systemd
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/lemon2graphite.service
%else
mkdir -p $RPM_BUILD_ROOT%{_initrddir}
install -m 755 %{SOURCE3} $RPM_BUILD_ROOT/%{_initrddir}/lemon2graphite
%endif
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/sysconfig/lemon2graphite
%config(noreplace) %{_sysconfdir}/lemon/carbon.csv
%{_sbindir}/lemon2graphite
%if %_want_systemd
  %{_unitdir}/lemon2graphite.service
%else
  %{_initrddir}/lemon2graphite
%endif

%if %_want_systemd
%post
%systemd_post lemon2graphite.service
%else
%post
/sbin/chkconfig --add lemon2graphite
/sbin/service  lemon2graphite condrestart 2>&1 || :
%endif

%if %_want_systemd
%preun
%systemd_preun lemon2graphite.service
%else
%preun
if [ $1 = 0 ]; then
  /sbin/service  lemon2graphite stop 2>&1 || :
  /sbin/chkconfig --del lemon2graphite
fi
%endif

%if %_want_systemd
%postun
%systemd_postun_with_restart lemon2graphite.service
%endif

%changelog
* Tue Aug  2 2016 Jan Iven <jan.iven@cern.ch>
- cope with oddly-formatted LEMON cache data

* Fri Jul  8 2016 Jan Iven <jan.iven@cern.ch>
- handle short hostnames

* Thu Apr 14 2016 Jan Iven <jan.iven@cern.ch>
- ignore socket errors when sending to CARBON, set 30s timeout

* Thu Dec  3 2015 Jan Iven <jan.iven@cern.ch>
- Initial build.
