SPECFILE = lemon2graphite.spec
FILES  = lemon2graphite.py carbon.csv lemon2graphite.init  lemon2graphite.sysconfig lemon2graphite.systemd
rpmtopdir := $(shell rpm --eval %_topdir)
rpmbuild  := $(shell [ -x /usr/bin/rpmbuild ] && echo rpmbuild || echo rpm)

PKGNAME = $(shell grep -s '^Name:'    $(SPECFILE) | sed -e 's/Name: *//')
PKGVERS = $(shell grep -s '^Version:' $(SPECFILE) | sed -e 's/Version: *//')

sources: $(FILES)

rpm: $(SPECFILE) $(FILES)
		mkdir -p $(rpmtopdir)/{SOURCES,SPECS,BUILD,SRPMS,RPMS}
		(tar cvfz $(rpmtopdir)/SOURCES/$(PKGNAME)-$(PKGVERS).tgz $(FILES))
		cp -vf $(FILES)  $(rpmtopdir)/SOURCES/
		cp -vf $(SPECFILE) $(rpmtopdir)/SPECS/$(SPECFILE)
		$(rpmbuild) -ba $(SPECFILE)
