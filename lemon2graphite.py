#!/usr/bin/python
#
# get list of LEMON metrics, read last value for these,
# convert to Grafana format, send to carbon DB host

import yaml
import sys
import argparse
import glob
import os
import socket
import pickle
import struct
import time

hostgroup = 'hg_undefined'
metric_file='/etc/lemon/carbon.csv'
metric_metadata='/etc/lemon/forwarder/metric_metadata.yml'
lemon_spool_dir='/var/spool/lemon-agent'
# no sense to loop any quicker
daemon_loop = 60

CARBON_PREFIX = 'test.lemon'
CARBON_PORT = 2004
CARBON_TIMEOUT = 30
PIDFILE = '/var/run/lemon2graphite.pid'

# Todo:
# should open a given LEMON metric file only once, read all fields on interest in one go
#  - requires different structure (from current flat tuple)

def read_lemon_metrics_config(filename):
    f=open(metric_metadata)
    meta=yaml.safe_load(f)  # this is *quite* slow?
    f.close()

    all_metrics=[]
    f=open(filename)
    for line in f:
        if line.startswith('#'):
            continue
        line=line.rstrip()
        if not line:
            continue            
        (metricid,field_ent) = line.split(',',1)
        try:
            (field,entity) = field_ent.split(',',1)
        except ValueError:
                field = field_ent
                entity = ''
        metricid=int(metricid)
        field=int(field)-1
        try:
            metricname = meta[metricid]['name']
            fieldname = meta[metricid]['fields'][field]['name']
            fieldtype = meta[metricid]['fields'][field]['type']
            if fieldtype not in ['INTEGER','FLOAT']:
                print "invalid/non-numeric metric %d field %s: is a %s" % (metricid, fieldname,fieldtype)
                continue
            fieldscale = meta[metricid]['fields'][field]['scale']
            if not fieldscale or fieldscale == 'None':
                fieldscale = 1.0
            else:
                fieldscale = float(fieldscale)
            # do we need to add key fields?
            keys=0
            for i in meta[metricid]['fields']:
                if not i['is_key']:
                    break
                else:
                    keys +=1
            if args.verbose:
                print "# Metric %d:%d=%s.%s, %d keys, for '%s'" % (metricid,field,metricname,fieldname,keys,entity)
            all_metrics.append([entity,metricid,metricname,field,fieldname,keys,fieldscale])
        except (IndexError, KeyError):
            print "metric %d:%d not configured on this machine" %(metricid,field)
    return all_metrics


def read_lemon_metric(one_metric):
    # the node might have several instances of this metric (e.g. reporting for clusters)
    # common case: LEMON reports for (short) hostname, we qualify this with the "hostgroup" parameter

    (instance, id, metricname, field, fieldname, keys, scale) = one_metric
    output = []

    if '*' in instance:
        for foundfile in glob.glob("%s/%s/last.%08d" %(lemon_spool_dir, instance, id)):
            # get the instance name from the directory name
            right=foundfile.rindex('/')
            left=foundfile.rindex('/',0,right)
            instance=foundfile[left+1:right]
            instance=instance.replace('.','_')  # replace potential separators
            # if the instance is actually just this host, use same subtree as for the host metrics
            if instance == myhost:
                instance = hostgroup+'.'+myhost
            if args.prefix:
                instance =args.prefix+'.'+instance
            output += read_one_lemon_metricfile(instance, foundfile, one_metric)
    else:
        if instance:
            metricfile = "%s/%s/last.%08d" %(lemon_spool_dir, instance, id)
        else:
            metricfile = "%s/%s/last.%08d" %(lemon_spool_dir, myhost, id)
            instance = hostgroup+'.'+myhost
        if args.prefix:
            instance =args.prefix+'.'+instance
        output += read_one_lemon_metricfile(instance, metricfile, one_metric)
    return output

def  read_one_lemon_metricfile(instance, metricfile, one_metric):
    (_unused, metricid, metricname, field, fieldname, keys, scale) = one_metric
    output = []
    now = time.time()
    try:
        f=open(metricfile)
    except IOError:
        print  "no data found for %d:%d=%s.%s" % (metricid,field,metricname,fieldname)
        return output
    for line in f:
        line = line.rstrip()
        if not line or line.startswith('#'):
          continue
        try:
          # format:  time^Ikey1 key2 val1 val2
          (timestamp, rest) = line.split("\t")
          # do not bother sending stuff that is too old (and has been sent before)
          if not args.nodaemon:
              if now - float(timestamp) > daemon_loop:
                  if args.verbose:
                      print "# ignoring stale data for %s.%s" % (metricname,fieldname)
                  continue
          values = rest.split(' ')
          # we might have to qualify the (numeric) value with one or more (text?) key fields
          # these go at end (so that a plot at leaf level will e.g. show all disks)
          optkeyvalue=''
          for k in range(keys):
            # might need to sanitize the keys, in particular don't want '/' as seen on mountpoints
            optkeyvalue += '.'+(values[k].translate(None,'./'))
          num_metric = float(values[field]) * scale

          data = ('.'.join([instance, metricname, fieldname + optkeyvalue]),
                ( int(timestamp), num_metric))

          output.append(data)
        except ValueError:
          continue  #ignore wonky lines..
    f.close()
    return output

def parse_args():
    parser = argparse.ArgumentParser(description=
                                     'Pump local LEMON metrics to Grafana',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog="""
Input file format: one metric per line, comma-separated:

metricID,fieldIndex[,instance]

  - metricID is numeric
  - LEMON field index starts at 1
  - 'instance' can be left empty (defaults to current host), or contain '*' wildcard


Output path into Carbon:

[prefix.][hostgroup.]hostname.metricname.fieldname[.key1[.key2..]] timestamp value
[prefix.]instance.metricname.fieldname[.key1[.key2..]] timestamp value

""")
    parser.add_argument('-v','--verbose', dest='verbose', action='store_true')
    
    parser.add_argument('--metricfile',
                        default=metric_file,
                        help="File with LEMON metrics to send  [%s]" % (metric_file))

    parser.add_argument('--nodaemon',
                        action='store_true',
                        help="Stay in foreground")
    parser.add_argument('--pidfile',
                        action='store_true',
                        help="Location of the PID file ['%s']" % (PIDFILE),
                        default=PIDFILE)

    parser.add_argument('--prefix',
                        default=CARBON_PREFIX,
                        help="store data in Carbon under this prefix ['%s']" % (CARBON_PREFIX))
    parser.add_argument('--hostgroup',
                        help="gets added to Carbon prefix for host metrics ['%s']" % (hostgroup),
                        default=hostgroup)

    # destination
    parser.add_argument('--port',
                        help='Carbon port [%d]' %( CARBON_PORT ), default=CARBON_PORT)
    parser.add_argument('--server',
                        help='Carbon server',
                        required=True)
    args = parser.parse_args()
    return args

def do_one_round(all_metrics):       
    # collect one round of all metrics
    output = []
    for one_metric in all_metrics:
        output += read_lemon_metric(one_metric)

    if not output:
        print "no data found"
        return
    if args.verbose:
        import pprint
        print "# will send"
        pprint.pprint(output)
    # 'pickle' and send off in one go
    payload = pickle.dumps(output, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload

    try:
        (family, socktype, proto, canonname, sockaddr) = socket.getaddrinfo(args.server, args.port, 0, 0, socket.IPPROTO_TCP)[0]
        CARBON = socket.socket(family, socktype, proto)
        CARBON.settimeout(CARBON_TIMEOUT)
        CARBON.connect(sockaddr)
        if args.verbose:
           print("#connected to "+args.server+":"+str(args.port))

        CARBON.sendall(message)
        CARBON.close()
    except socket.error, exc:
          print "Caught exception socket.error : %s" % exc

if __name__ == '__main__':
    try:
        (myhost,_discard) = os.uname()[1].split('.',1)  # aka "short hostname"
    except ValueError:
        myhost = os.uname()[1]

    args=parse_args()
    hostgroup = args.hostgroup.replace('/','.')
    
    # which metrics do we want to forward?
    all_metrics = read_lemon_metrics_config(args.metricfile)

    if not args.nodaemon:
        import daemon
        try:
            import daemon.pidfile
            pidfile=daemon.pidfile.PIDLockFile(args.pidfile)
        except ImportError:   # not supported on SLC6..
            import lockfile
            pidfile=lockfile.FileLock(args.pidfile)
        # keep output in verbose mode. Or use logging.
        daemon_stdout = None
        daemon_stderr = None
        if args.verbose:
            daemon_stdout = sys.stdout
            daemon_stderr = sys.stderr
        with daemon.DaemonContext(stdout=daemon_stdout,
                                  stderr=daemon_stderr,
                                  detach_process=True,
                                  pidfile=pidfile):
            while True:
                do_one_round(all_metrics)
                time.sleep(daemon_loop)
        
    else:
        do_one_round(all_metrics)
